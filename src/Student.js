import React from "react";
import Marks from "./Marks";



export default class Student extends React.Component
{
    componentDidMount()
   {
        console.log("Student- component didmounted !!!");
        
   }

    componentWillUnmount()
    { 
      console.log("Student- component willUnmount !!! (runs just before component is removed)");
      
    }

    render(){
         console.log("Student - Rendered [child of app]");
        return (
        <div>
        <h1>Hello {this.props.name}</h1> 
        <Marks/>
        </div>
        );
    }
}