import React from "react";
import Student from "./Student";


export default class LifeCycle extends React.Component
{
   constructor(props){
       super(props);
       console.log('App- constructor called !!!');
       console.log(props);
      //  this.state = {
      //    roll:"101",
      //  };
       this.state = {count: 1};
    }

   static getDerivedStateFromProps(props,state)
   {
     console.log("APP- getDerivedStateFromProps !!!");
     console.log(props,state);
     return null;
     //return {message: "updated text"}
   }
   
   componentDidMount()
   {
        console.log("APP- component didmounted !!!");
        
   }
    
    clickHandle(){
         console.log("Button Pressed !!!");
        //  this.setState({roll:102});
         
    }

    shouldComponentUpdate(nextProps, nextState) {
      console.log("APP- should componentUpdate !!!");
      
    if (this.props.color !== nextProps.color) {
      return true;
    }
    if (this.state.count !== nextState.count) {
      return true;
    }
    return false;
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState)
    {
      console.log("APP- Snapshot taken !!! (It runs before update)");
      console.log(prevProps, prevState);
      return null;
    }

    componentDidUpdate(prevProps,prevState,Snapshot)
    {
      console.log("APP- Component didUpdated !!! (It runs after update)");
      console.log(prevProps, prevState,Snapshot);
    }

    

    render(){
        console.log("APP- rendered !!!");
        return (
            <div>
              <Student name ="HIMANSHU JOSHI" />   
              <button onClick={this.clickHandle}>Click</button> 
              <h1>Count: {this.state.count}</h1>
              <button
               onClick={() => this.setState(state => ({count: state.count + 1}))}>Counter
              </button>
            </div>
        );    
    }

  
}

