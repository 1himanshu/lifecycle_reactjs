import React from 'react';
import ReactDOM from 'react-dom';
import LifeCycle from './LifeCycle';
import Student from './Student';

ReactDOM.render(<LifeCycle name ='i_Am_props' />, document.getElementById('root'));
ReactDOM.render(<Student />, document.getElementById('stu'));
ReactDOM.unmountComponentAtNode(document.getElementById('stu'));

